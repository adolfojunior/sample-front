import thunkMiddleware from 'redux-thunk'
import promiseMiddleware from 'redux-actions-promise'
import { routerMiddleware } from 'react-router-redux'

import history from '../history'

const historyMiddleware = routerMiddleware(history)

const loggerMiddleware = ({ getState }) => next => action => {
  const { type } = action
  /*eslint-disable */
  try {
    console.group(type)
    console.info(`dispatching`, action)
    return next(action)
  } finally {
    console.log(`next state`, getState())
    console.groupEnd(type)
  }
}

export default [
  thunkMiddleware,
  promiseMiddleware,
  loggerMiddleware,
  historyMiddleware,
]
