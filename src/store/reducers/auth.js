import { handleActions } from 'redux-actions'

import { login, userAuth } from '../../actions/auth'

// SELECTORS
export const getAuth = ({ auth }) => auth

// REDUCERS
export default handleActions({
  [login]: (state, { payload }) => {
    console.log(`will try to login user:`, payload)
    return payload
  },
  [userAuth]: (state, { payload }) => {
    console.log(`user auth:`, payload)
    return payload
  },
}, null)
