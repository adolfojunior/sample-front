import { combineReducers } from 'redux'
import { routerReducer } from 'react-router-redux'

import auth from './auth'

// REDUCER
export default combineReducers({
  auth,
  routerReducer
})
