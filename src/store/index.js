import { createStore, applyMiddleware, compose } from 'redux'

import reducers from './reducers'
import middleware from './middleware'

export default createStore(
  reducers,
  compose(
    applyMiddleware(...middleware),
    window.devToolsExtension ? window.devToolsExtension() : f => f,
  ),
)