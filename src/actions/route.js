import { createAction } from 'redux-actions'
import { push } from 'react-router-redux'

const navigateTo = (page) => (dispatch) => dispatch(push(page))

export const homePage = createAction('HOME_PAGE', () => navigateTo(`/`))
export const loginPage = createAction('LOGIN_PAGE', () => navigateTo(`/login`))
