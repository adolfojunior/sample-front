import { createAction } from 'redux-actions'
import { loginPage, homePage } from './route'

import authApi from '../api/auth'

export const userAuth = createAction('USER-AUTH')

export const login = createAction('LOGIN', (username, password) => async (dispatch) => {
  try {
    
    const user = await authApi.login(username, password)

    dispatch(userAuth(user))
    dispatch(homePage())
  } catch(e) {
    dispatch(userAuth(e))
    dispatch(loginPage())
  }
})
