
const delay = (timeout) => new Promise(resolve => setTimeout(resolve, timeout))

const login = async (username, password) => {
  
  await delay(2000)

  if (username !== password) {
    throw new Error(`Invalid user`)
  }

  return {
    username
  }
}

export default {
  login
}