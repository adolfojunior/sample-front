import React from 'react'
import Menu from './Menu'

const Home = () => (
  <div>
    <Menu/>
    <h2>Home</h2>
  </div>
)

export default Home
