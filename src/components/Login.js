import React from 'react'
import PropTypes from 'prop-types'
import { Link } from 'react-router-dom'
import { connect } from 'react-redux'
import { push } from 'react-router-redux'

import { login } from '../actions/auth'
import Menu from './Menu'

class Login extends React.Component {

  static propTypes = {
    auth: PropTypes.object
  }

  bindRef(name) {
    return input => this[name] = input
  }

  handleLogin = () => {
    const { username, password } = this
    this.props.dispatch(login(username.value, password.value))
  }

  render() {
    const { match: { params } } = this.props
    return (
      <div>
        <Menu/>
        <input type="text" ref={this.bindRef(`username`)} />
        <input type="text" ref={this.bindRef(`password`)} />
        <input type="button" value="login" onClick={this.handleLogin} />
        <hr/>
        <Link to={`../`}>Back</Link>
      </div>
    )
  }
}

export default connect(
  ({ auth }) => ({ auth })
)(Login)