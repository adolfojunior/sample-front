import React from "react"
import { Link } from "react-router-dom"

export default class UserList extends React.Component {
  render() {
    const { match } = this.props
    return (
      <div>
        <h3>Please select a user.</h3>
        <ul>
          <li>
            <Link to={`${match.url}/1`}>User 1</Link>
          </li>
          <li>
            <Link to={`${match.url}/2`}>User 2</Link>
          </li>
        </ul>
      </div>
    )
  }
}
