import React from "react"
import { Route, Link } from "react-router-dom"

import UserList from "./UserList"
import UserForm from "./UserForm"

const Users = ({ match }) => (
  <div>
    <h2>Users</h2>
    <hr/>
    <Route exact path={match.url} component={UserList}/>
    <Route path={`${match.url}/:id`} component={UserForm} />
  </div>
)

export default Users