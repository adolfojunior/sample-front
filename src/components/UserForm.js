import React from "react"
import { Link } from "react-router-dom"

export default class UserForm extends React.Component {
  render() {
    const { match } = this.props
    const { params } = match
    return (
      <div>
        <h3>Form User {params.id}</h3>
        <input value={params.id}/>
        <hr/>
        <Link to={`../users`}>Back to list</Link>
      </div>
    )
  }
}
