import React from 'react'
import { Switch, Route } from 'react-router'

import Home from '../components/Home'
import Users from '../components/Users'
import Login from '../components/Login'

const App = () => (
  <Switch>
    <Route exact path="/" component={Home} />
    <Route path="/users" component={Users} />
    <Route path="/login" component={Login} />
  </Switch>
)

export default App